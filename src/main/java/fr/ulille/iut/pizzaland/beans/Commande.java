package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {
	private long id;
	private String nom;
	private String prenom;
	private List<Pizza> pizzas;
	
	public Commande(String nom, String prenom, List<Pizza> pizzas) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.pizzas = pizzas;
	}
	
	public Commande() {
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public List<Pizza> getPizzas() {
		return pizzas;
	}
	
	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public static Commande fromDto(CommandeDto dto) {
		Commande c = new Commande();
		c.setNom(dto.getNom());
		c.setPrenom(dto.getPrenom());
		c.setId(dto.getId());
		c.setPizzas(dto.getPizzas());
		
		return c;
	}
	
	public static CommandeDto toDto(Commande c) {
		CommandeDto dto = new CommandeDto();
		dto.setNom(c.getNom());
		dto.setPrenom(c.getPrenom());
		dto.setId(c.getId());
		dto.setPizzas(c.getPizzas());
		
		return dto;
	}

	public static Commande fromCreateDto(CommandeCreateDto dto) {
		Commande c = new Commande();
		
		c.setNom(dto.getNom());
		c.setPrenom(dto.getPrenom());
		c.setPizzas(dto.getPizzas());
		
		return c;
	}
}