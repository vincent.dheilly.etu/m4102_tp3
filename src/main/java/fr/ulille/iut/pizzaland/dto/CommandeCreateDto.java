package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeCreateDto {
	private String nom;
	private String prenom;
	private List<Pizza> pizzas;
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public List<Pizza> getPizzas() {
		return pizzas;
	}
	
	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
}
