package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

@Path("/pizzas")
public class PizzaResource {
	private PizzaDao pizzas;
	
	 @Context
	 public UriInfo uriInfo;
	 
	public PizzaResource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTable();
	}
	
	@GET
	public List<PizzaDto> getAll(){
		 return pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
	}
	
	@GET
    @Path("{id}")
    public PizzaDto getOnePizza (@PathParam("id") long id) {
    	try {
    		Pizza p = pizzas.findPizza(id);
    		return Pizza.toDto(p);
    	}
    	catch(Exception e) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    }
	
	@GET
    @Path("{id}/name")
    public String getOnePizzaName (@PathParam("id") long id) {
    	try {
    		Pizza p = pizzas.findPizza(id);
    		return p.getName();
    	}
    	catch(Exception e) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    }
	
	@POST
    public Response createIngredient(PizzaCreateDto dto) {
    	Pizza existing = pizzas.findByName(dto.getName());
    	
        if ( existing != null ) {
            							throw new WebApplicationException(Response.Status.CONFLICT);
        }
        
        try {
            Pizza pizza = Pizza.fromCreateDto(dto);
            long id = pizzas.insert(pizza.getName(),pizza.getIngredients());
            pizza.setId(id);
            
            PizzaDto pDto = Pizza.toDto(pizza);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(pDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
	}
	
	@DELETE
    @Path("{id}")
    public Response deletePizza(@PathParam("id") long id) {
    	if ( pizzas.findPizza(id) == null ) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}

        pizzas.remove(id);

        return Response.status(Response.Status.ACCEPTED).build();
    }

}
