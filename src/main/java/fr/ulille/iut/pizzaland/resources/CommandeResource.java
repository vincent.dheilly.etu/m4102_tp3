package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

@Path("/commandes")
public class CommandeResource {
	private CommandeDao commandes;

	@Context
    public UriInfo uriInfo;

	@GET
	public List<CommandeDto> getAll(){
		 return commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
	}
	
	@POST
    public Response createCommande(CommandeCreateDto dto) {
    	/*Commande existing = commandes.findByName(dto.getNom());
    	
        if ( existing != null ) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }*/
        
        try {
            Commande com = Commande.fromCreateDto(dto);
            long id = commandes.insert(com.getNom(),com.getPrenom(),com.getPizzas());
            com.setId(id);
            
            CommandeDto pDto = Commande.toDto(com);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(pDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
	}
}
