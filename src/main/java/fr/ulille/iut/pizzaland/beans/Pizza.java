package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> ingredients;
	
	public Pizza() {}
	
	
	public Pizza(long id, String name, List<Ingredient> ingredients) {
		this.id = id;
		this.name = name;
		this.ingredients = ingredients;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Ingredient> getIngredients() {
		return ingredients;
	}
	
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setName(p.getName());
		dto.setIngredients(p.getIngredients());
		return dto;
	}
	
	public static Pizza fromDto(PizzaDto dto) {
		Pizza p = new Pizza();
		p.setId(dto.getId());
		p.setName(dto.getName());
		p.setIngredients(dto.getIngredients());
		return p;
	}
	
	public static Pizza fromCreateDto(PizzaCreateDto dto) {
		Pizza p = new Pizza();
		p.setName(dto.getName());
		p.setIngredients(dto.getIngredients());
		return p;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
