package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	IngredientDao ingredientsDao = (IngredientDao) BDDFactory.buildDao(IngredientDao.class);
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id INTEGER PRIMARY KEY, name VARCHAR NOT NULL)")
	public void  createTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (pizza INTEGER,"
																	+ "ingredient INTEGER,"
																	+ "PRIMARY KEY(pizza,ingredient),"
																	+ "FOREIGN KEY(pizza) REFERENCES Pizzas(id),"
																	+ "FOREIGN KEY(ingredient) REFERENCES ingredients(id))")
	public void createAssociationTable();

	@SqlUpdate("INSERT INTO Pizzas (name) VALUES (:name)")
	@GetGeneratedKeys
	public long insertPizza(String name);
	
	@SqlUpdate("INSERT INTO PizzaIngredientsAssociation VALUES (:pizza, :ingredient)")
	public void associatePizzaIngre(long pizza, long ingredient);
	
	public default long insert(String name, List<Ingredient> ingredients) {
		long pizza = insertPizza(name);
		
		for(Ingredient i : ingredients) {
			associatePizzaIngre(pizza,i.getId());
		}
		
		return pizza;
	}
	
	@SqlQuery("SELECT * FROM Pizzas")
	@RegisterBeanMapper(Pizza.class)
	public List<Pizza> getPizzas();

	public default List<Pizza> getAll(){
		List<Pizza> pizzas = getPizzas();
		
		for(Pizza p : pizzas) {
			p.setIngredients(ingredientsDao.getOfPizza(p.getId()));
		}
		
		return pizzas;
	}
	
	@SqlQuery("SELECT * FROM Pizzas as p , CommandesAndPizzas as ass WHERE p.id = ass.pizza AND ass.commande = :commande")
	@RegisterBeanMapper(Pizza.class)
	public List<Pizza> getOfCommande(long commande);
	
	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);
	
	@SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);
	
	public default Pizza findPizza(long id) {
		Pizza p = findById(id);
		
		if(p != null)
			p.setIngredients(ingredientsDao.getOfPizza(p.getId()));
		
		return p;
	}
	
	@Transaction
	public default void createTableAndIngredientAssociation() {
	    createTable();
	    createAssociationTable();
	}

	@SqlUpdate("DELETE FROM PizzaIngredientsAssociation WHERE pizza = :pizza")
	void removeAss(long pizza);
	
	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void removePizza(long id);
	
	public default void remove(long id) {
		removeAss(id);
		removePizza(id);
	}
}
