package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {
	PizzaDao pizzaDao = (PizzaDao) BDDFactory.buildDao(PizzaDao.class);
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS Commandes (id INTEGER PRIMARY KEY, firstname VARCHAR NOT NULL, lastname VARCHAR NOT NULL)")
	public void  createTable();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS CommandesAndPizzas (pizza INTEGER,"
			+ "commande INTEGER,"
			+ "PRIMARY KEY(pizza,commande),"
			+ "FOREIGN KEY(pizza) REFERENCES Pizzas(id),"
			+ "FOREIGN KEY(commande) REFERENCES commandes(id))")
	public void createAssociationTable();
	
	public default void createTableAndAss() {
		createTable();
		createAssociationTable();
	}
	
	@SqlUpdate("INSERT INTO commandes (firstname, lastname) VALUES (:nom, :prenom)")
	@GetGeneratedKeys
	public long insertCommande(String nom, String prenom);
	
	@SqlUpdate("INSERT INTO CommandesAndPizzas VALUES (:pizza, :commande)")
	public void associateCommandePizza(long pizza, long commande);
	
	public default long insert(String nom, String prenom, List<Pizza> pizzas) {
		long commande = insertCommande(nom,prenom);
		
		for(Pizza p : pizzas) {
			associateCommandePizza(p.getId(),commande);
		}
		
		return commande;
	}

	@SqlQuery("SELECT * FROM commandes")
	@RegisterBeanMapper(Commande.class)
	public List<Commande> getCommandes();

	public default List<Commande> getAll(){
		List<Commande> commandes = getCommandes();
		
		for(Commande c : commandes) {
			c.setPizzas(pizzaDao.getOfCommande(c.getId()));
		}
		
		return commandes;
	}
}
